package com.kenfogel.fish_fxhtml_demo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kenfogel.fish_fxhtml_demo.persistence.FishDAO;
import com.kenfogel.fish_fxhtml_demo.persistence.FishDAOImpl;
import com.kenfogel.fish_fxhtml_demo.view.FishFXHTMLController;
import java.io.IOException;

public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    private Stage primaryStage;
    private BorderPane fishFXHTMLLayout;
    private FishDAO fishDAO;

    public MainApp() {
        super();
        fishDAO = new FishDAOImpl();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Fish HTML Editor");

        // Set the application icon using getResourceAsStream.
        this.primaryStage.getIcons().add(
                new Image(MainApp.class
                        .getResourceAsStream("/images/bluefish_icon.png")));

        initRootLayout();
        primaryStage.show();
    }

    /**
     * Load the layout and controller.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class
                    .getResource("/fxml/FishFXHTMLLayout.fxml"));
            fishFXHTMLLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(fishFXHTMLLayout);
            primaryStage.setScene(scene);

            FishFXHTMLController controller = loader.getController();
            controller.setFishDAO(fishDAO);
            controller.displayFishAsHTML();
            //controller.displayOtherHTML();

        } catch (IOException e) {
            LOG.error("Error displaying html editor", e);
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
