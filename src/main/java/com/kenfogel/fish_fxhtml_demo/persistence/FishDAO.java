package com.kenfogel.fish_fxhtml_demo.persistence;

import com.kenfogel.fish_fxhtml_demo.beans.FishData;

import java.sql.SQLException;
import java.util.ArrayList;

import javafx.collections.ObservableList;

/**
 * Interface for CRUD methods
 *
 * @author Ken
 */
public interface FishDAO {

    public ObservableList<FishData> findTableAll() throws SQLException;
    // Create

    public int create(FishData fishData) throws SQLException;

    // Read
    public ArrayList<FishData> findAll() throws SQLException;

    public FishData findID(int id) throws SQLException;

    public ArrayList<FishData> findDiet(String diet) throws SQLException;

    // Update
    public int update(FishData fishData) throws SQLException;

    // Delete
    public int delete(int ID) throws SQLException;
}
