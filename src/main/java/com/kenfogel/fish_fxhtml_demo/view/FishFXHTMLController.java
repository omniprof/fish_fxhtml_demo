package com.kenfogel.fish_fxhtml_demo.view;

import java.sql.SQLException;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.HTMLEditor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kenfogel.fish_fxhtml_demo.beans.FishData;
import com.kenfogel.fish_fxhtml_demo.persistence.FishDAO;
import javafx.collections.ObservableList;

public class FishFXHTMLController {

    private final static Logger LOG = LoggerFactory.getLogger(FishFXHTMLController.class);

    private FishDAO fishDAO;

    @FXML
    private BorderPane fishFXHTMLLayout;

    @FXML
    private HTMLEditor fishFXHTMLEditor;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded. Not much to do here.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Saves the file to the person file that is currently open. If there is no
     * open file, the "save as" dialog is shown.
     */
    @FXML
    private void handleSave() {
        System.out.println(fishFXHTMLEditor.getHtmlText());
    }

    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        // Modal dialog box
        Alert dialog = new Alert(AlertType.INFORMATION);
        dialog.setTitle("JavaFX HTML Demo");
        dialog.setHeaderText("About");
        dialog.setContentText("JavaFX HTMLEditor Demo");
        dialog.show();
    }

    /**
     * Closes the application.
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }

    /**
     * Sets a reference to the FishDAO object that retrieves data from the
     * database. Convert the first three fields from the first three records
     * into HTML.
     *
     * @param fishDAO
     */
    public void setFishDAO(FishDAO fishDAO) {
        this.fishDAO = fishDAO;
    }

    public void displayFishAsHTML() {
        ObservableList<FishData> data;
        try {
            data = fishDAO.findTableAll();
        } catch (SQLException e) {
            LOG.error("Error retrieving records: ", e.getCause());
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("<html><body contenteditable='false'>");
        for (int x = 0; x < 3; ++x) {
            sb.append(data.get(x).getId()).append("</br>");
            sb.append(data.get(x).getCommonName()).append("</br>");
            sb.append(data.get(x).getLatin()).append("</br></br>");
        }
        sb.append("</body></html>");

        fishFXHTMLEditor.setHtmlText(sb.toString());
    }

    public void displayOtherHTML() {
        String other = "<html><META http-equiv=Content-Type content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is my photograph embedded in this email.</h1><img src=\""
                + getClass().getResource("/FreeFall.jpg") + "\"><h2>I'm flying!</h2></body></html>";

        fishFXHTMLEditor.setHtmlText(other);
    }

}
